<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
        $per_page = 5;
        $page = \Input::get('page');
        if(empty($page)):
            $page = 1;
        endif;

        $query = \DB::select()->from('books');

        if(!empty(\Input::get('s_category'))):
            $query->where('category_id', \Input::get('s_category'));
        endif;
        if(!empty(\Input::get('s_title'))):
            $query->where('title', 'LIKE', '%'.\Input::get('s_title').'%');
        endif;
        if(!empty(\Input::get('page'))):
            $query->offset(($page - 1) * $per_page);
        endif;

        $data['books'] = $query->limit($per_page)
            ->execute();
        
        $data['categories'] = \DB::select() ->from('categories')
            ->execute();
        
        $view = View::forge('bookshelf/index');
        $view->set('books', $data['books']);
        $view->set('categories', $data['categories']);
        return $view;
		return Response::forge(View::forge('bookshelf/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
    
    public function action_review($books_id)
    {
        $data['books'] = \DB::select()->from('books')
           ->where('id', $books_id)
           ->execute()->current();

        $view = View::forge('welcome/review');
        $view->set('books', $data['books']);
        return Response::forge($view);
    }
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
    
    public function action_edit()
    {
        return Response::forge(View::forge('welcome/edit'));
    }
    
    public function action_new()
    {
        return Response::forge(Presenter::forge('welcome/new'));
    }
    
    public function action_show($books_id)
    {
        $data['books'] = \DB::select()->from('books')
            ->where('id', $books_id)
            ->execute()->current();
        
        $view = View::forge('welcome/show');
        $view->set('books',$data['books']);
        return Response::forge($view);
    }
    
    public function action_delete($books_id)
    {
         \DB::delete('books')
            ->where('id', $book_id)
            ->execute();
        
        return Response::redirect('/');
    }
    
        
    public function action_create($book_id)
    {
        
        $data['books'] = \DB::select()->from('books')
           ->where('id', $book_id)
           ->execute()->current();
        
        \DB::insert('books')
            ->set(array(
                 'title' => 'title1',
                 'author' => 'author1',
                 'category_id' => 100,
                 ))
            ->execute();
        
        return
             Response::redirect('/');
    }
}

