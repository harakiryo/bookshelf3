<?php

class Controller_mybook extends Controller{
    
    
    public function action_index()
    {
        
        
        $data['mybooks'] = \DB::select('*',array('mybooks.id','mybooks_id'))->from('mybooks')
            ->join('books')
            ->on('mybooks.book_id','=', 'books.id')
            ->execute();

        $view = View::forge('mybook/mybook');
        $view->set('mybooks', $data['mybooks']);
        return $view;
		return Response::forge(View::forge('mybook/mybook'));
    }
    
    public function action_review($mybook_id)
    {
        $data['mybooks'] = \DB::select()->from('mybooks')
            ->join('books')
            ->on('mybooks.book_id','=', 'books.id')
           ->where('mybooks.id', $mybook_id)
           ->execute()->current();

        $view = View::forge('mybook/review');
        $view ->set('mybooks', $data['mybooks']);
        return Response::forge($view);
        
    }
    
    public function post_review($mybook_id)
    {
        
        
        \DB::Insert('review')->
            set(array(
        'review' => \Input::post('review'),
        'book_id' => $mybook_id ,
        'user_id' => 1,
            ))
            ->execute();
        
        
        return Response::redirect('/reviews');
    }
    
    public function action_edit($mybook_id)
    {
       $data['mybooks'] = \DB::select()->from('mybooks')
           ->join('books')
           ->on('mybooks.book_id','=','books.id')
           ->where('mybooks.id', $mybook_id)
           ->execute()->current();
        
        $data['categories'] = \DB::select()->from('categories')
            ->execute();
        
        $view = View::forge('mybook/edit');
        $view->set('mybooks', $data['mybooks']);
        $view->set('categories' , $data['categories']);
        return Response::forge($view);
    }
    
    public function post_edit ($mybook_id)
    {
        \DB::update('mybooks')
            ->where('mybooks.book_id', $mybook_id)
            ->set(array(
                 'title' => \Input::post('title'),
                 'author' => \Input::post('author'),
                
                
                 ))
            ->execute();
        
        \DB::update('books')
            ->where('id', $mybook_id)
            ->set(array(
                 'title' => \Input::post('title'),
                 'author' => \Input::post('author'),
                'category_id' => \Input::post('category_id'),
                 ))
            ->execute();
        
        return Response::redirect('/mybook');
    }
    
    public function action_delete($mybook_id)
    {
        \DB::delete('mybooks')
            ->where('mybooks.id',$mybook_id)
            ->execute();
        
        return Response::redirect('/mybook');
    }
}