<?php

class Controller_bookshelf extends Controller
{

    public function action_index()
	{
       
        
        $result = DB::select()->from('books')->execute();
        $count = count($result);
        
        
       $config = array(
       'pagination_url' => 'bookshelf/index',
        'total_items' =>$count,
           'per_page' =>5,
           'uri_segment' =>3,
           
       );
        
        $pagination = Pagination::forge('mypagination' , $config);
        
        $data['books'] = DB::select()
        ->from('books')
        ->limit($pagination->per_page)
        ->offset($pagination->offset)
            ->execute()
            ->as_array();
        
        $data['pagination'] = $pagination;
        
        
        $data['categories'] = DB::select()->from('categories')
            ->execute();
        
        
        return \View::forge('bookshelf/index', $data,false);
        
        
    }
    public function action_review($books_id)
    {
        $data['books'] = \DB::select() ->from('books')
            ->where('id' , $books_id)
            ->execute()->current();
        

        $view = View::forge('bookshelf/review');
        $view->set('books', $data['books']);
        return Response::forge($view);
    }
    public function post_review($books_id)
    {
        
        \DB::Insert('review')
            ->set(array(
                 'review' => \Input::post('review'),
                 'book_id' =>  $books_id,
                'user_id' => Arr::get(Auth::get_user_id(),1),
                 ))
            ->execute();
        
        return Response::redirect('/');
    }
    
    public function action_edit($book_id)
    {
        $data['books'] = \DB::select()->from('books')
           ->where('id', $book_id)
           ->execute()->current();
        
        $data['categories'] = \DB::select() ->from('categories')
            ->execute();
        
              
         $view = View::forge('bookshelf/edit');
        $view->set('books', $data['books']);
        $view->set('categories' , $data['categories']);
        
		return Response::forge($view);
   
        
    }
    
    public function post_edit($review_id)
    {
        
        \DB::update('books')
            ->where('id', $review_id)
            ->set(array(
                 'title' => \Input::post('title'),
                 'author' => \Input::post('author'),
                 'category_id' => \Input::post('category_id'),
                
                 ))
            ->execute();
        
        return Response::redirect('/');
    }
    public function action_delete($book_id)
    {
        \DB::delete('books')
            ->where('id', $book_id)
            ->execute();

        return Response::redirect('/');
    }
    
    public function action_newbook()
    {
        $data['categories'] = \DB::select() ->from('categories')
            ->execute();
        
        $view->set('categories' , $data['categories']);
        return Response::forge(View::forge('bookshelf/newbook'));
    }

    
    public function action_create($book_id)
    {
        $data['books'] = \DB::select()->from('books')
           ->where('id', $book_id)
           ->execute()->current();
        
        $data['categories'] = \DB::select() ->from('categories')
            ->execute();
        
        
          $view = View::forge('bookshelf/newbook');
        $view->set('books', $data['books']);
        $view->set('categories' , $data['categories']);
        
		return Response::forge($view);
        
        
             
    }
    
    public function post_create($review_id)
    {
        
        $result = \DB::Insert('books')
            ->set(array(
                 'title' => \Input::post('title'),
                 'author'=> \Input::post('author'),
                  'category_id' => \Input::post('category_id'),
                 ))
            ->execute();
        
        \DB::Insert('mybooks')
            ->set(array(
                 'title' => \Input::post('title'),
                 'author' => \Input::post('author'),
                
                 'user_id' => Arr::get(Auth::get_user_id(),1),
                 'book_id' => $result[0],
                 ))
            ->execute();
        
        
        
        return Response::redirect('/');
    }
    public function action_show($review_id)
    {
        $data['review'] = \DB::select()->from('review')
           ->where('id', $review_id)
           ->execute()->current();

        $view = View::forge('bookshelf/show');
        $view->set('review', $data['review']);
        return Response::forge($view);

    }
    
    
public function action_login() 
 { 
 $data = array(); 
 if ($_POST) 
 { 
 $auth = Auth::instance(); 

 if ($auth->login($_POST['email'],$_POST['password'])) 
 { 
 Response::redirect('/'); 
 } 
 else 
 { 

 $data['usename'] = $_POST['username']; 

 $data['login_error'] = 'ユーザー名かパスワードが違います。再入力して下さい。'; 
 } } 
    
 echo View::forge('bookshelf/login',$data); 
 } 
    
 
public function action_logout() 
 { 
 Auth::logout(); 
 echo View::forge('bookshelf/login'); 
 } 


 


public function action_add_user() 
 { 
 if ($_POST)
 { 
 $username=Input::post('username'); 
 $password=Input::post('password'); 
 $email=Input::post('email');
 $auth = Auth::instance(); 
 $auth->create_user($username,$password,$email); 
 } 
 echo View::forge('bookshelf/add_user'); 
 }
}
