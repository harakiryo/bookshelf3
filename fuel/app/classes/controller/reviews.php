<?php



class Controller_reviews extends Controller{
 
    
    
    public function action_index()
    {
        
        $result = \DB::select()->from('review')->execute();
        $count = count($result);
        
        $config = array(
       'pagination_url' =>'reviews',
        'total_items' =>$count,
           'per_page' =>5,
           'uri_segment' =>2,
           
       );
        
         $pagination = Pagination::forge('mypagination' , $config);
        
        
        $data['reviews'] = \DB::select('*',array('review.id','id'))->from('review')
            ->join('users')
            ->on('review.user_id','=','users.id' )
             ->join('books')
           ->on('review.book_id','=','books.id')
             ->limit($pagination->per_page)
        ->offset($pagination->offset)
            ->execute()
            ->as_array();
           
         
        $data['pagination'] = $pagination;
       
        
        
        
       

        $view = View::forge('review/review');
        $view->set('reviews', $data['reviews']);
        
          return \View::forge('review/review', $data,false);
    }
    
    public function action_edit($review_id)
   {

       $data['review'] = \DB::select('*',array('review.id','id'))->from('review')
           ->join('books')
           ->on('review.book_id','=','books.id')
           ->where('review.id', $review_id)
           ->execute()->current();
    
        
       
            
        

       $view = View::forge('review/edit');
       $view->set('review', $data['review']);
        
       return Response::forge($view);
   } 
    
     public function post_edit($review_id)
    {
        \DB::update('review')
            ->where('review.id', $review_id)
            ->set(array(
                'review' => \Input::post('review'),
                 ))
            ->execute();
        
        return Response::redirect('/reviews');
    }
    
    public function action_delete($review_id)
    {
        
         \DB::delete('review')
            ->where('id', $review_id)
            ->execute();
        
        return Response::redirect('/reviews');
    }
    
    
    
   
}