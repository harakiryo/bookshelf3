<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="pYaQjXcG8Dt25uPDSf6ZlxB6YMsbrRCdnnD/kP0cxPIeYIS5D/LrWD+jKHYzuVpARqp0Wd3FSg1eGiXJnYuwcg==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></a></li>
              <li><a href="/my_books">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">アカウント <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/users/edit">設定</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a rel="nofollow" data-method="delete" href="/users/sign_out">ログアウト</a></li>
                </ul>
              </li>
            </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      
<div class="panel panel-default devise-bs">
  <div class="panel-heading">
    <h4>ユーザ編集</h4>
  </div>
  <div class="panel-body">
    <form class="edit_user" id="edit_user" action="/users" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="put" /><input type="hidden" name="authenticity_token" value="vr1DfQITHODBuxIb16gtnU7sm+0aZELrpAkrLsDILmMB8GJcYCr32Zbxl/iHyBeuqMsyeLlXgBvf4IzT6XCTag==" />
      <div class="form-group">
        <label for="user_name">名前</label>
        <input autofocus="autofocus" class="form-control" type="text" value="原木凌" name="user[name]" id="user_name" />
      </div>
      <div class="form-group">
        <label for="user_email">メールアドレス</label>
        <input class="form-control" type="email" value="lingyuanmu63@gmail.com" name="user[email]" id="user_email" />
      </div>
      <div class="form-group">
        <label for="user_password">パスワード</label> <i>(空欄のままなら変更しません)</i>
        <input autocomplete="off" class="form-control" type="password" name="user[password]" id="user_password" />
      </div>
      <div class="form-group">
        <label for="user_password_confirmation">確認用パスワード</label>
        <input class="form-control" type="password" name="user[password_confirmation]" id="user_password_confirmation" />
      </div>
      <div class="form-group">
        <label for="user_current_password">現在のパスワード</label> <i>(変更を反映するには現在のパスワードを入力してください)</i>
        <input class="form-control" type="password" name="user[current_password]" id="user_current_password" />
      </div>
      <input type="submit" name="commit" value="更新" class="btn btn-primary" data-disable-with="更新" />
</form>  </div>
</div>

<p>気に入りません? <a data-confirm="本当に良いですか?" rel="nofollow" data-method="delete" href="/users">アカウント削除</a>.</p>

<a href="https://itpro-book-shelf.herokuapp.com/">Back</a>

    </div>
  </body>
</html>
