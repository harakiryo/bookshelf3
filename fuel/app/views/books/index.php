<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="Y/6E6Id7n+G2XXpMLfT61kFup+bqMKolbNMNSEZl3rA6WrsW5Et2jXmbq2W7+xtsdnx+BpPgw2/tTG4E+2EXzg==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/js/bootstrap.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/bookshelf">本棚</a></a></li>
              <li><a href="/mybook">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">アカウント <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/users/edit">設定</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a rel="nofollow" data-method="delete" href="/users/sign_out">ログアウト</a></li>
                </ul>
              </li>
            </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      <h1>Books</h1>
<form action="/books" method="get" class="form-inline">
  <input class="form-control" type="text" name="s_title" value="">
  <select name="s_category" id="s_category" class="form-control"><option value="" label=" "></option><option value="1">文学・評論</option>
<option value="2">人文・思想</option>
<option value="3">社会・政治</option>
<option value="4">ノンフィクション</option>
<option value="5">歴史・地理</option>
<option value="6">ビジネス・経済</option>
<option value="7">投資・金融・会社経営</option>
<option value="8">科学・テクノロジー</option>
<option value="9">医学・薬学</option>
<option value="10">コンピュータ・IT</option>
<option value="11">アート・建築・デザイン</option>
<option value="12">趣味・実用</option>
<option value="13">スポーツ・アウトドア</option>
<option value="14">資格・検定・就職</option>
<option value="15">暮らし・健康・子育て</option>
<option value="16">旅行ガイド・マップ</option>
<option value="17">語学・辞事典・年鑑</option>
<option value="18">教育・学参・受験</option>
<option value="19">絵本・児童書</option>
<option value="20">コミック・ラノベ・BL</option>
<option value="21">ライトノベル</option>
<option value="22">タレント写真集</option>
<option value="23">ゲーム攻略・ゲームブック</option>
<option value="24">エンターテイメント</option>
<option value="25">新書・文庫</option>
<option value="26">雑誌</option>
<option value="27">楽譜・スコア・音楽書</option>
<option value="28">ポスター</option>
<option value="29">アダルト</option>
<option value="30">カレンダー</option></select>
  <button class="btn btn-default" type="submit">検索</button>
</form>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Title</th>
      <th>Author</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach($books as $book): ?>
      <tr>
        <td><?php echo $book['title']; ?></td>
        <td><?php echo $book['author']; ?></td>
        <td class="text-right">
          <a class="btn btn-link btn-xs" href="/bookshelf/review/<?php echo $book['id']; ?>">Review</a>
          <a class="btn btn-link btn-xs" href="/bookshelf/edit/<?php echo $book['id']; ?>">Edit</a>
          <a class="btn btn-xs btn-danger" data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/books/1">Delete</a>
        </td>
      </tr>
     <?php endforeach; ?>
  </tbody>
</table>


  <nav>
    <ul class="pagination">
      
      
            <li class="page-item active">
    <a remote="false" class="page-link">1</a>
  </li>

            <li class="page-item">
    <a rel="next" class="page-link" href="/?page=2">2</a>
  </li>

            <li class="page-item">
    <a class="page-link" href="/?page=3">3</a>
  </li>

            <li class="page-item">
    <a class="page-link" href="/?page=4">4</a>
  </li>

            <li class="page-item">
    <a class="page-link" href="/?page=5">5</a>
  </li>

      <li class="page-item">
  <a rel="next" class="page-link" href="/?page=2"><span class="translation_missing" title="translation missing: ja.views.pagination.next">Next</span></a>
</li>

      <li class="page-item">
  <a class="page-link" href="/?page=5"><span class="translation_missing" title="translation missing: ja.views.pagination.last">Last</span></a>
</li>

    </ul>
  </nav>

<a class="btn btn-primary" href="/books/new">New Book</a>

    </div>
  </body>
</html>
