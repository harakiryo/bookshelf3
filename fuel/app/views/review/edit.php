<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="QWJoOrTNuUJ0z5fXCJ0vc/iapEieJswRNO+97NYBD/wRqmJpC5ec1SZbNBoXfLgQveT6vZgiZlzcVxHQKHSaMg==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></li>
              <li><a href="/mybook">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              
                  <li><a rel="nofollow" data-method="delete" href="bookshelf/logout">ログアウト</a></li>
                </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      <h1>New Review</h1>

<h2> <?php echo $review['title']; ?></h2>
<form novalidate="novalidate" class="simple_form new_review" id="new_review" action="/reviews/edit/<?php echo $review['id']; ?>" accept-charset="UTF-8" method="post">
    <input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="W/1JbBNsQECqHNcEMJPxfKMSiP/Ip/tqUsQo6fIrMMpJmRre4KiEpfceruFXB0HhhGk2+mk/kYRLo2y7zp3t/Q==" />
    
  <div class="form-group text optional review_description"><label class="control-label text optional" for="review_description">Description</label><textarea class="form-control text optional" name="review" id="review_description" value=""><?php echo $review['review']; ?>
</textarea></div>
  <div class="form-actions">
    <input type="submit" name="commit" value="Update Review" class="btn btn-default btn-primary" data-disable-with="Update Review" />
  </div>
</form>

<a href="/reviews">Back</a>

    </div>
  </body>
</html>


     
       
     
  