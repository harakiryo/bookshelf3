<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="Dh1t2HMQZXxMj0ndJYwTfSm7t0Pmc4nfY0weaEploWtMZaUIK+R6My/mR5DuC7MAXE1tt+poePKve6bnb/ElNw==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></li>
              <li><a href="/mybook">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
           <ul class="nav navbar-nav navbar-right">
              
                  <li><a rel="nofollow" data-method="delete" href="bookshelf/logout">ログアウト</a></li>
                </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      <h1>Reviews</h1>
<table class="table table-striped">
  <thead>
    <tr>
      <th>User</th>
      <th>Book</th>
      <th>Description</th>
      <th ></th>
    </tr>
  </thead>
  <tbody>
      <?php foreach($reviews as $review): ?>
     
      <tr>
       
        <td><?php echo $review['username']; ?></td>
        <td><?php echo $review['title']; ?></td>
        <td><?php echo $review['review']; ?></td>
        <td class="text-right">
          <a class="btn btn-link btn-xs" href="/reviews/edit/<?php echo $review['id']; ?>">Edit</a>
          <a class="btn btn-xs btn-danger" data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/reviews/delete/<?php echo $review['id']; ?>">Delete</a>
        </td>
      </tr>
    
      <?php endforeach; ?>
     

  </tbody>
</table>
        
      <?php echo $pagination->render(); ?>

      </div>
    </body>
</html>