<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="PhNqETFfUYkIH7XfXUgXYifhtsU7gu3e5VkP1HoGU4qBUv1aElBrJnCKTnIEw9h9/zCCSz8nN9LuLTpIWcZKEQ==" />

    <link rel="stylesheet" media="all" href="/assets/css/botstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></a></li>
              <li><a href="/my_books">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">アカウント <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/users/edit">設定</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a rel="nofollow" data-method="delete" href="/users/sign_out">ログアウト</a></li>
                </ul>
              </li>
            </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      <p id="notice"></p>
<?php foreach($books as $book): ?>
<p>
  <strong>Title:</strong>
  <?php echo $book['title']; ?>
</p>

<p>
  <strong>Author:</strong>
  <?php echo $book['author']; ?>
</p>
<?php endforeach; ?>
<a href="/books/1/edit">Edit</a> |
<a href="/">Back</a>

    </div>
  </body>
</html>