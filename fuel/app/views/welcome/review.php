<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="Dh1t2HMQZXxMj0ndJYwTfSm7t0Pmc4nfY0weaEploWtMZaUIK+R6My/mR5DuC7MAXE1tt+poePKve6bnb/ElNw==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></li>
              <li><a href="/mybook">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">アカウント <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/users/edit">設定</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a rel="nofollow" data-method="delete" href="/users/sign_out">ログアウト</a></li>
                </ul>
              </li>
            </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      <h1>Reviews</h1>
<table class="table table-striped">
  <thead>
    <tr>
      <th>User</th>
      <th>Book</th>
      <th>Description</th>
      <th ></th>
    </tr>
  </thead>
  <tbody>
      <?php foreach($reviews as $review): ?>
      <tr>
        <td><?php echo $review['title']; ?></td>
        <td><?php echo $review['review']; ?></td>
        <td class="text-right">
          <a class="btn btn-link btn-xs" href="/reviews/edit/<?php echo $review['ID']; ?>">Edit</a>
          <a class="btn btn-xs btn-danger" data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/books/1">Delete</a>
        </td>
      </tr>
     <?php endforeach; ?>

  </tbody>
</table>

  <nav>
    <ul class="pagination">
      
      
            <li class="page-item active">
    <a remote="false" class="page-link">1</a>
  </li>

            <li class="page-item">
    <a rel="next" class="page-link" href="/reviews?page=2">2</a>
  </li>

      <li class="page-item">
  <a rel="next" class="page-link" href="/reviews?page=2"><span class="translation_missing" title="translation missing: ja.views.pagination.next">Next</span></a>
</li>

      <li class="page-item">
  <a class="page-link" href="/reviews?page=2"><span class="translation_missing" title="translation missing: ja.views.pagination.last">Last</span></a>
</li>

    </ul>
  </nav>


    </div>
  </body>
</html>