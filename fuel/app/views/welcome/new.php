<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="7HQ1A1fhHw+J1JiPWRUkZBARhhGA88vgHVRc2Xb/pVRVmUTDfXYwAEfyCWM5G/kYeDRVEw8W6Su6yu/7jwlknw==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></a></li>
              <li><a href="/mybook">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">アカウント <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/users/edit">設定</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a rel="nofollow" data-method="delete" href="/users/sign_out">ログアウト</a></li>
                </ul>
              </li>
            </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      <h1>New Book</h1>

<form novalidate="novalidate" class="simple_form new_book" id="new_book" action="/books" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="7tIG/gQjsvI2ycHYDPFIfdWBcvAofg6jxtqhERFy8dwPA9IGciQMGwzjDU1BF/TYsj8fbfzc4GJHKAj9q1C8dw==" />
  <div class="form-group string required book_title"><label class="control-label string required" for="book_title"><abbr title="required">*</abbr> タイトル</label><input class="form-control string required" type="text" name="book[title]" id="book_title" /></div>
  <div class="form-group string optional book_author"><label class="control-label string optional" for="book_author">著者</label><input class="form-control string optional" type="text" name="book[author]" id="book_author" /></div>
  <div class="form-group select optional book_categories"><label class="control-label select optional" for="book_category_ids">Categories</label><input name="book[category_ids][]" type="hidden" value="" /><select class="form-control select optional" multiple="multiple" name="book[category_ids][]" id="book_category_ids"><option value="1">文学・評論</option>
<option value="2">人文・思想</option>
<option value="3">社会・政治</option>
<option value="4">ノンフィクション</option>
<option value="5">歴史・地理</option>
<option value="6">ビジネス・経済</option>
<option value="7">投資・金融・会社経営</option>
<option value="8">科学・テクノロジー</option>
<option value="9">医学・薬学</option>
<option value="10">コンピュータ・IT</option>
<option value="11">アート・建築・デザイン</option>
<option value="12">趣味・実用</option>
<option value="13">スポーツ・アウトドア</option>
<option value="14">資格・検定・就職</option>
<option value="15">暮らし・健康・子育て</option>
<option value="16">旅行ガイド・マップ</option>
<option value="17">語学・辞事典・年鑑</option>
<option value="18">教育・学参・受験</option>
<option value="19">絵本・児童書</option>
<option value="20">コミック・ラノベ・BL</option>
<option value="21">ライトノベル</option>
<option value="22">タレント写真集</option>
<option value="23">ゲーム攻略・ゲームブック</option>
<option value="24">エンターテイメント</option>
<option value="25">新書・文庫</option>
<option value="26">雑誌</option>
<option value="27">楽譜・スコア・音楽書</option>
<option value="28">ポスター</option>
<option value="29">アダルト</option>
<option value="30">カレンダー</option></select></div>
  <div class="actions">
    <input type="submit" name="commit" value="Create Book" class="btn btn-default btn-primary" data-disable-with="Create Book" />
  </div>
</form>

<a href="/">Back</a>

    </div>
  </body>
</html>