

<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="uRZ/cqfX3PerjHtf6y2JljbL2Hz0cBFl1twozGg/pSovBCDTFuYYibvd7xMyQmqMs/Ufs3TJ24JoEeUwCvG6IQ==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
      </div>
    </nav>
    <div class="container">
      
<div class="panel panel-default devise-bs">
  <div class="panel-heading">
    <h4>アカウント登録</h4>
  </div>
  <div class="panel-body">
    <form role="form" class="new_user" id="new_user" action="" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="nkMf59ew+s+YhcpKDu5ZYUrv9IYTWlCgekbQHvngJbs+2Uam0nRMpfx/4zsiOFZRfFQDIOzIjthZG0JuO8BiCA==" />
      <div class="form-group">
        <label for="user_name">名前</label>
        <input autofocus="autofocus" class="form-control" type="text" name="username" id="user_name" />
      </div>
      <div class="form-group">
        <label for="user_email">メールアドレス</label>
        <input class="form-control" type="email" value="" name="email" id="user_email" />
      </div>
      <div class="form-group">
        <label for="user_password">パスワード</label>
        <input class="form-control" type="password" name="password" id="user_password" />
      </div>
      <div class="form-group">
        <label for="user_password_confirmation">確認用パスワード</label>
        <input class="form-control" type="password" name="password_confirmation" id="user_password_confirmation" />
      </div>
      <input type="submit" name="commit" value="アカウント登録" class="btn btn-primary" data-disable-with="アカウント登録" />
</form>  </div>
</div>
  <a href="/users/sign_in">ログイン</a><br />


  <a href="/users/password/new">パスワードを忘れましたか?</a><br />





    </div>
  </body>
</html>