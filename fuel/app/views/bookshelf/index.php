<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="sIMZ1X68VALA4pGRooOP+aPrfvdQZNMmmNG7cvjFYCbgSxOGweZxlZJ2Mly9Yhia5pUgAlZgeWtwaRdOBrD16A==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></li>
              <li><a href="/mybook">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              
                  <li><a rel="nofollow" data-method="delete" href="bookshelf/logout">ログアウト</a></li>
                </ul>
             
          </div>
      </div>
    </nav>
    <div class="container">
      <h1>Books</h1>
<form action="/books" method="get" class="form-inline">
  <input class="form-control" type="text" name="s_title" value="">
  <select name="category_name" id="s_category" class="form-control"><option value="" label=" "></option>
      
      
      <?php foreach($categories as $category): ?>
      <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
      <?php endforeach; ?>
    </select>

  <button class="btn btn-default" type="submit">検索</button>
</form>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Title</th>
      <th>Author</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach($books as $book): ?>
      <tr>
        <td><?php echo $book['title']; ?></td>
        <td><?php echo $book['author']; ?></td>
        <td class="text-right">
          <a class="btn btn-link btn-xs" href="/bookshelf/review/<?php echo $book['id']; ?>">Review</a>
          <a class="btn btn-link btn-xs" href="/bookshelf/edit/<?php echo $book['id']; ?>">Edit</a>
          <a class="btn btn-xs btn-danger" data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/bookshelf/delete/<?php echo $book['id']; ?>">Delete</a>
        </td>
      </tr>
     <?php endforeach; ?>
             

        
  </tbody>
</table>

<?php echo $pagination->render(); ?>
        
          
        

<a class="btn btn-primary" href="/bookshelf/create/<?php echo $book['id']; ?>">New Book</a>

    </div>
  </body>
</html>