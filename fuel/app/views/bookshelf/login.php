
<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="QiqM+xkf/8rv1sl6/AKJysnqwvDdWdGxfTMxyudkb5bjNePkpcCha1mNV/Wl1KCbcWuQ5ZkPv26I1IL2EyRlZw==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
      </div>
    </nav>
    <div class="container">
        <div class="alert alert-dismissable alert-danger">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <p>アカウント登録もしくはログインしてください。</p>
        </div>
      <div class="panel panel-default devise-bs">
  <div class="panel-heading">
    <h4>ログイン</h4>
  </div>
  <div class="panel-body">
    <form role="form" class="new_user" id="new_user" action="/" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="sTUMBFZad3Xo2xJfUtfAhYOQUIkdjtHhMNbq83/dQQRvvmHlQLeJsO/O7iXS6C+MMecWaAWMep23YVNhsZW4Kw==" />
      <div class="form-group">
        <label for="user_email">メールアドレス</label>
        <input autofocus="autofocus" class="form-control" type="email" value="" name="email" id="user_email" />
      </div>
      <div class="form-group">
        <label for="user_password">パスワード</label>
        <input autocomplete="off" class="form-control" type="password" name="password" id="user_password" />
      </div>
        <div class="checkbox">
          <label for="user_remember_me">
            <input name="user[remember_me]" type="hidden" value="0" /><input type="checkbox" value="1" name="user[remember_me]" id="user_remember_me" /> Remember me
</label>        </div>
      <input type="submit" name="commit" value="ログイン" class="btn btn-primary" data-disable-with="ログイン" />
</form>  </div>
</div>

  <a href="/users/sign_up">アカウント登録</a><br />

  <a href="/users/password/new">パスワードを忘れましたか?</a><br />





    </div>
  </body>
</html>

