<!DOCTYPE html>
<html>
  <head>
    <title>BookShelf</title>
    <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="1NdyIoSozCZ8DpWggKna8vgiqvhFedyV83kUHTbf/kKEH3hxO/LpsS6aNm2fSE2RvVz0DUN9dtgbwbghyKprjA==" />

    <link rel="stylesheet" media="all" href="/assets/css/bootstrap.css" data-turbolinks-track="reload" />
    <script src="/assets/application-8bb1cd5a3a9c37f559d478bcb83497db022902042c78074fe4b9fd8e659fa9fb.js" data-turbolinks-track="reload"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          <a class="navbar-brand" href="/">BookShelf</a>
        </div>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="/">本棚</a></li>
              <li><a href="/mybook">マイ本棚</a></li>
              <li><a href="/reviews">レビュー</a></li>
            </ul>
           
              <ul class="nav navbar-nav navbar-right">
              
                  <li><a rel="nofollow" data-method="delete" href="bookshelf/logout">ログアウト</a></li>
                </ul>
          </div>
      </div>
    </nav>
    <div class="container">
      <h1>Editing Book</h1>

<form novalidate="novalidate" class="simple_form edit_book" id="edit_book_1"  accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="_method" value="patch" /><input type="hidden" name="authenticity_token" value="x5Pk6t+vA5erZX9q057t3ziQ+q8quLOIl0gUWB6Giu7J3TTS5ZPoy4R//SftgJebNJGCHivNjs0PqzY6HHqL9A==" />
  <div class="form-group string required book_title"><label class="control-label string required" for="book_title"><abbr title="required">*</abbr> タイトル</label><input class="form-control string required" type="text" value="<?php echo $books['title']; ?>" name="title" id="book_title" /></div>
  <div class="form-group string optional book_author"><label class="control-label string optional" for="book_author">著者</label><input class="form-control string optional" type="text" value="<?php echo $books['author']; ?>" name="author" id="book_author" /></div>
  <div class="form-group select optional book_categories"><label class="control-label select optional" for="book_category_ids">Categories</label><input name="category_name" type="hidden" value="" /><select class="form-control select optional"  name="category_id" id="book_category_ids">
      
      
      <?php foreach($categories as $category): ?>
      <option selected="selected" value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
     <?php endforeach; ?>
      
      
      
</select></div>
   
  <div class="actions">
    <input type="submit" name="commit" value="Update Book" class="btn btn-default btn-primary" data-disable-with="Update Book" />
  </div>
</form>

<a href="/bookshelf/show/<?php echo $books['id']; ?>">Show</a> |
<a href="/">Back</a>

    </div>
  </body>
</html>